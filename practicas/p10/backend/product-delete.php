<?php
    use API\Eliminar\Eliminar as Eliminar;
    require_once __DIR__ . '/API/start.php';

    // SE VERIFICA HABER RECIBIDO EL ID
    if( isset($_POST['id']) ) {
        $id_obj = $_POST['id'];
        $producto = new Delete('marketzone');

        $producto->delete($id_obj);
    } 
    
    $producto->get_response();
?>