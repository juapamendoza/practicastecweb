<?php
    use API\Leer\Leer as Leer;
    require_once __DIR__ . '/API/start.php';

    // SE VERIFICA HABER RECIBIDO LA BUSQUEDA
    if( isset($_GET['search']) ) {
        $search = $_GET['search'];
        
        $producto = new Leer('marketzone');
        $producto->search($search);
    } 
    
    $producto->get_response();
?>