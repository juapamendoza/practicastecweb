<?php
namespace API\DataBase;
use mysqli;

    abstract class DataBase{
        protected $conexion;
        protected $response;

        public function __construct($nombre_bd){
            $this->conexion = new mysqli("localhost", "root", "", "{$nombre_bd}");
            //echo 'Conexion establecida :)';
            $this->response = array();
            if ($this->conexion->connect_errno) {
                echo 'No se puede conectar a la BD :( ' . $this->conexion->connect_error;
                exit;
            }
        }

        public function get_response(){
            $respuesta_json = json_encode($this->response, JSON_PRETTY_PRINT);
            echo $respuesta_json;
        } //fin funcion get_response
    }

    //probar la conexion
    //$conex = new DataBase;
    //$conex->DataBase('marketzone');
?>
