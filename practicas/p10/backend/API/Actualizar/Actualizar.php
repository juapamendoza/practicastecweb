<?php
    namespace API\Actualizar;

    require_once __DIR__ . '/../DataBase/DataBase.php';
    use API\DataBase\DataBase as DataBase;

    class Actualizar extends DataBase{
        public function edit($objeto){
            //$producto = file_get_contents('php://input');
            $this->response = array(
                'status'  => 'error',
                'message' => 'La consulta falló'
            );
            if(!empty($objeto)) {
                // SE TRANSFORMA EL STRING DEL JASON A OBJETO
                $jsonOBJ = json_decode($objeto);

                $idobj = $jsonOBJ->id;
                $nombreobj = $jsonOBJ->nombre;
                $precioobj = $jsonOBJ->precio;
                $unidadesobj = $jsonOBJ->unidades;
                $modeloobj = $jsonOBJ->modelo;
                $marcaobj = $jsonOBJ->marca;
                $detallesobj = $jsonOBJ->detalles;
                $imagenobj = $jsonOBJ->imagen;

                // SE ASUME QUE LOS DATOS YA FUERON VALIDADOS ANTES DE ENVIARSE
                $sql = "UPDATE productos SET nombre='{$nombreobj}', precio='{$precioobj}', unidades='{$unidadesobj}', modelo='{$modeloobj}', marca='{$marcaobj}', detalles='{$detallesobj}', imagen='{$imagenobj}' WHERE id = '{$idobj}'";
                //$result = $this->conexion->query($sql);
                
                if ( $this->conexion->query($sql) ) {
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto modificado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
                // Cierra la conexion
                $this->conexion->close();
            }
        } //fin duncion edit
    }
?>