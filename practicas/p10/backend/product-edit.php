<?php
    use API\Actualizar\Actualizar as Actualizar;
    require_once __DIR__ . '/API/start.php';

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $info_producto = file_get_contents('php://input');

    $producto = new Actualizar('marketzone');
    $producto->edit($info_producto);

    $producto->get_response();
?>