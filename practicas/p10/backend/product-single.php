<?php
    use API\Leer\Leer as Leer;
    require_once __DIR__ . '/API/start.php';

    // SE VERIFICA HABER RECIBIDO EL ID
    if( isset($_POST['id']) ) {
        $id = $_POST['id'];
        
        $producto = new Leer('marketzone');
        $producto->single($id);
    } 
    
    $producto->get_response();
?>