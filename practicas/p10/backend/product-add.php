<?php
    use API\Crear\Crear as Crear;
    require_once __DIR__ . '/API/start.php';

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $info_producto = file_get_contents('php://input');
    
    $producto = new Crear('marketzone');
    $producto->add($info_producto);

    $producto->get_response();
?>