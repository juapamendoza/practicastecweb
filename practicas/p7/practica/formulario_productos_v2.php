<!DOCTYPE html >
<html>

  <head>
    <meta charset="utf-8" >
    <title>Modificación de producto</title>
    <style type="text/css">
      ol, ul { 
      list-style-type: none;
      }
    </style>
  </head>

  <body>
    <script type="text/javascript">
      function verificarNombre(nombre) {
        if (nombre.value == '') {
          alert('El nombre no puede estar vacío');
        }
        else if (nombre.lenght >= 100) {
          alert('El nombre no puede tener más de 100 caracteres');
        }
      }

      function verificarEstilo() {
        if (!document.getElementsByName('estilo').checked) {
          alert('Debes seleccionar un estilo');
        }
      }

      function verificarModelo(modelo) {
        if (modelo.value == '') {
          alert('No se especificó artista');
        }
        else if (modelo.lenght > 25) {
          alert('El nombre del artista es muy largo');
        }
      }

      function verificarPrecio(precio) {
        verifP = parseFloat(precio);
        if (precio.value == '') {
          alert('El producto debe tener un precio');
        }
        else if (verifP < 99.99) {
          alert('El precio debe ser mayor a $99.99');
        }
      }

      function verificarDetalles(detalles) {
        if (detalles.lenght > 250) {
          alert('Ingresa detalles con menos de 250 caracteres');
        }
      }

      function verificarUnidades(unidades) {
        parseInt(unidades);
        if (unidades.value < 0) {
          alert('Debes ingresar 0 o más unidades');
        }
      }

      function verificarImg(img) {
        if (img.value == '') {
          img.value = 'img/imagen.png';
        }
      }

    </script>
    <h1>Modificación del producto con ID <?=$_GET['id']?></h1>

    <!-- Formulario -->
    <form id="formularioTenis" action="http://localhost/tecnologiasweb/p7/practica/set_producto_v2.php" method="post">
      <h2>Información del Producto</h2>
        
      <fieldset>
          <ul>
              <li><label for="id-prod">ID:</label><input type="text" name="id" value="<?=$_GET['id']?>" readonly></li>
              <li><label for="nombre-prod">Nombre:</label><input type="text" name="nombre" id="nombre-prod" onblur="verificarNombre(this)" value="<?=$_GET['nombre']?>"></li>
              <p>Estilo:</p>
              <li><label for="poprock">Pop-Rock</label><input type="radio" name="estilo" id="poprock" value="Pop-Rock"></li>
              <li><label for="metal">Metal</label><input type="radio" name="estilo" id="metal" value="Metal"></li>
              <li><label for="indie">Indie-Alternativo</label><input type="radio" name="estilo" id="indie" value="Indie"></li>
              <li><label for="modelo-prod">Artista:</label><input type="text" name="modelo" id="modelo-prod" maxlength="25" onblur="verificarModelo(this)" value="<?=$_GET['modelo']?>"></li>
              <li><label for="precio-prod">Precio:</label><input type="text" name="precio" id="precio-prod" required onblur="verificarPrecio(this)"></li>
              <li><label for="detalles-prod">Detalles:</label><br><textarea name="detalles" id="detalles-prod" rows="2" cols="60" placeholder="(opcional)" onblur="verificarDetalles(this)"></textarea></li>
              <li><label for="unidades-prod">Unidades:</label><input type="text" name="unidades" id="unidades-prod" required onblur="verificarUnidades(this)" value="<?=$_GET['unidades']?>"></li>
              <li><label for="imagen-prod">Imagen:</label><input type="text" name="imagen" id="imagen-prod" placeholder="(opcional)" onblur="verificarImg(this)"></li>
              <li><label for="oculto-prod">Ocultar producto:</label></li>
              <select name="oculto" id="oculto-prod">
                <option value="1">Si</option>
                <option value="0" selected="yes">No</option>
              </select>
          </ul>
      </fieldset>

      <p>
          <input type="submit" value="Modificar producto" onclick="verificarEstilo()">
          <input type="reset">
      </p>

    </form>
  </body>
</html>