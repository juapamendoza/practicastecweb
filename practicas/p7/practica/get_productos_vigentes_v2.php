<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

    <?php
        //header("Content-Type: application/json; charset=utf-8");
        //$data = aray();

        //se hace el obj de conexion
        @$link = new mysqli('localhost', 'root', 'p0p0c1t4', 'marketzone');

        //comprobar conexion
        if ($link->connect_errno) 
        {
            die('Falló la conexión: '.$link->connect_error.'<br/>');
            //exit();
        }

        //crea una tabla y la guarda en $result
        if ( $result = $link->query("SELECT * FROM productos WHERE eliminado = 1") ) 
        {
            $conSQL = "SELECT * FROM productos WHERE eliminado = 1";
            $row = $result->fetch_all(MYSQLI_ASSOC); //arreglo
            $query = mysqli_query($link,$conSQL);
            //libera memoria de $result
            $result->free();
        }

        $link->close();
    ?>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <?php
            echo "<title>Productos Eliminados</title>";
        ?>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
	<body>
        <script>
            function obtenerDatos() {
                //obtener id de la fila
                var rowId = event.target.parentNode.parentNode.id;
                //obtener arreglo de datos
                var data = document.getElementById(rowId).querySelectorAll(".row-data");

                //declarar variables con la info
                var idform = data[0].innerHTML;
                var nombreform = data[1].innerHTML;
                var modeloform = data[3].innerHTML;
                var unidadesform = data[5].innerHTML;

                var urlform = "http://localhost/tecnologiasweb/p7/practica/formulario_productos_v2.php";
                window.open(urlform+"?id="+idform+"&nombre="+nombreform+"&modelo="+modeloform+"&unidades="+unidadesform);

                //mandarForm(idform, nombreform, marcaform, modeloform, precioform, unidadesform, detallesform, imgform);
            }
        </script>
        <?php
		    echo "<h3>PRODUCTOS ELIMINADOS DE FORMA LÓGICA</h3>";
        ?>

		<br/>

		<table class="table">
			<thead class="thead-dark">
				<tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
		    	<th scope="col">Marca</th>
				<th scope="col">Modelo</th>
				<th scope="col">Precio</th>
				<th scope="col">Unidades</th>
				<th scope="col">Detalles</th>
				<th scope="col">Imagen</th>
                <th scope="col">Modificar Producto</th>
				</tr>
			</thead>
			<tbody>
                <?php foreach ($query as $objeto): ?> 
                <tr id="<?=$objeto['id']?>">
                <th class="row-data"><?= $objeto['id'] ?></th>
						<td class="row-data"><?= $objeto['nombre'] ?></td>
						<td class="row-data"><?= $objeto['marca'] ?></td>
						<td class="row-data"><?= $objeto['modelo'] ?></td>
						<td class="row-data"><?= $objeto['precio'] ?></td>
						<td class="row-data"><?= $objeto['unidades'] ?></td>
						<td class="row-data"><?= utf8_encode($objeto['detalles']) ?></td>
						<td class="row-data"><img src=<?= $objeto['imagen'] ?> widht="150px" height="150px"></td>
                        <td><input type="button"
                                   value="Editar Objeto"
                                   onClick="obtenerDatos()"/></td>
				</tr>
                <?php endforeach; ?>
			</tbody>
		</table>
    </body>
</html>