<?php
$nombre = $_POST['nombre'];
$marca = $_POST['marca'];
$modelo = $_POST['modelo'];
$precio = $_POST['precio'];
$detalles = $_POST['detalles'];
$unidades = $_POST['unidades'];
$imagen = $_POST['imagen'];
$eliminado = 0;

// crear conexion
@$link = new mysqli('localhost', 'root', 'p0p0c1t4', 'marketzone');	

// comprobar conexion
if ($link->connect_errno) 
{
    die('Falló la conexión: '.$link->connect_error.'<br/>');
}

// comprobar que no este vacio algun campo
if (!empty($nombre) && !empty($marca) && !empty($modelo) && !empty($precio) && !empty($detalles) && !empty($unidades) && !empty($imagen)) {
    if (is_float($precio + 0)) {
        if (is_int($unidades + 0)) {
            //echo "se puede agregar";
            $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', '{$eliminado}')";
            if ( $link->query($sql) ) 
            {
                echo 'Producto insertado con ID: '.$link->insert_id;
            }
            else
            {
                echo 'ERROR: El Producto no pudo ser insertado :(';
            }

            $link->close();
        }
        else {
            echo "ERROR: el valor de unidades debe ser entero";
        }
    }
    else {
        echo "ERROR: el precio no es decimal";
    }
}
else {
    echo "ERROR: faltan campos por llenar";
}
?>