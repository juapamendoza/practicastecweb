<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

    <?php
        //header("Content-Type: application/json; charset=utf-8");
        //$data = aray();

        //se hace el obj de conexion
        @$link = new mysqli('localhost', 'root', 'p0p0c1t4', 'marketzone');

        //comprobar conexion
        if ($link->connect_errno) 
        {
            die('Falló la conexión: '.$link->connect_error.'<br/>');
            //exit();
        }

        //crea una tabla y la guarda en $result
        if ( $result = $link->query("SELECT * FROM productos WHERE eliminado = 1") ) 
        {
            $conSQL = "SELECT * FROM productos WHERE eliminado = 1";
            $row = $result->fetch_all(MYSQLI_ASSOC); //arreglo
            $query = mysqli_query($link,$conSQL);
            //libera memoria de $result
            $result->free();
        }

        $link->close();
    ?>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <?php
            echo "<title>Productos Eliminados</title>";
        ?>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
	<body>
        <?php
		    echo "<h3>PRODUCTOS ELIMINADOS DE FORMA LÓGICA</h3>";
        ?>

		<br/>

		<table class="table">
			<thead class="thead-dark">
				<tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
		    	<th scope="col">Marca</th>
				<th scope="col">Modelo</th>
				<th scope="col">Precio</th>
				<th scope="col">Unidades</th>
				<th scope="col">Detalles</th>
				<th scope="col">Imagen</th>
				</tr>
			</thead>
			<tbody>
                <?php foreach ($query as $objeto): ?> 
                <tr>
						<th scope="row"><?= $objeto['id'] ?></th>
						<td><?= $objeto['nombre'] ?></td>
						<td><?= $objeto['marca'] ?></td>
						<td><?= $objeto['modelo'] ?></td>
						<td><?= $objeto['precio'] ?></td>
						<td><?= $objeto['unidades'] ?></td>
						<td><?= utf8_encode($objeto['detalles']) ?></td>
						<td><img src=<?= $objeto['imagen'] ?>></td>
				</tr>
                <?php endforeach; ?>
			</tbody>
		</table>
    </body>
</html>