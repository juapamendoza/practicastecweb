// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
  };

// FUNCIÓN CALLBACK DE BOTÓN "Buscar"
function buscarCadena(e) {
    e.preventDefault();

    // SE OBTIENE LA CADENA A BUSCAR
    var search_cad = document.getElementById('search').value;

    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/read.php', true);
    client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    client.onreadystatechange = function () {
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log('[CLIENTE]\n'+client.responseText);
            
            // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
            let productos = JSON.parse(client.responseText);    // similar a eval('('+client.responseText+')');
            let template = '';

            for(var i = 0; i<productos.length; i++) {
                 // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                let descripcion = '';
                descripcion += '<li>precio: '+productos[i].precio+'</li>';
                descripcion += '<li>unidades: '+productos[i].unidades+'</li>';
                descripcion += '<li>modelo: '+productos[i].modelo+'</li>';
                descripcion += '<li>marca: '+productos[i].marca+'</li>';
                descripcion += '<li>detalles: '+productos[i].detalles+'</li>';
                    
                // SE CREA UNA PLANTILLA PARA CREAR LA(S) FILA(S) A INSERTAR EN EL DOCUMENTO HTML
                template += `
                    <tr>
                        <td>${productos[i].id}</td>
                        <td>${productos[i].nombre}</td>
                        <td><ul>${descripcion}</ul></td>
                    </tr>
                `;

                 // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                document.getElementById("productos").innerHTML = template;
                //document.getElementById("productos").appendChild(template);
            }
        } 
    };
    client.send("search_cad="+search_cad);
}

function verificarNombre(nombre) {
    if (nombre.value == '') {
      alert('El nombre no puede estar vacío');
    }
    else if (nombre.lenght >= 100) {
      alert('El nombre no puede tener más de 100 caracteres');
    }
  }

// FUNCIÓN CALLBACK DE BOTÓN "Agregar Producto"
function agregarProducto(e) {
    e.preventDefault();

    // SE OBTIENE DESDE EL FORMULARIO EL JSON A ENVIAR
    var productoJsonString = document.getElementById('description').value;
    // SE CONVIERTE EL JSON DE STRING A OBJETO
    var finalJSON = JSON.parse(productoJsonString);

    // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
    finalJSON['nombre'] = document.getElementById('name').value;
    // SE OBTIENE EL STRING DEL JSON FINAL
    productoJsonString = JSON.stringify(finalJSON,null,2);

    var validar = 0;
    //validacion de datos
    if (finalJSON['nombre'] == '') {
        validar = 1; 
        alert('Debes escribir un nombre');  
    }
    if (finalJSON['precio'] < 99.99) {
        validar = 1;
        alert('El precio deber ser mayor a $99.99');
        
    }
    if (finalJSON['unidades'] < 0) {
        validar = 1;
        alert('Debes ingresar 0 o más unidades');
    }
    if (finalJSON['modelo'] == '') {
        validar = 1;
        alert('Debes especificar el modelo');
    }
    if (finalJSON['marca'] == 'NA' || finalJSON['marca'] == '') {
        validar = 1;
        alert('Debes especificar una marca');
    }
    if (finalJSON['detalles'].lenght > 250) {
        validar = 1;
        alert('Detalles muy largos');
    }
    if (finalJSON['imagen'] == '') {
        finalJSON['imagen'] = "img/imagen.png";
    }

    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/create.php', true);
    client.setRequestHeader('Content-Type', "application/json;charset=UTF-8");
    client.onreadystatechange = function () {
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log(client.responseText);
            window.alert(client.responseText);
        }
    };
    if (validar == 0) {
        client.send(productoJsonString);    
    }
}

// SE CREA EL OBJETO DE CONEXIÓN COMPATIBLE CON EL NAVEGADOR
function getXMLHttpRequest() {
    var objetoAjax;

    try{
        objetoAjax = new XMLHttpRequest();
    }catch(err1){
        /**
         * NOTA: Las siguientes formas de crear el objeto ya son obsoletas
         *       pero se comparten por motivos historico-académicos.
         */
        try{
            // IE7 y IE8
            objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
        }catch(err2){
            try{
                // IE5 y IE6
                objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
            }catch(err3){
                objetoAjax = false;
            }
        }
    }
    return objetoAjax;
}

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;
}