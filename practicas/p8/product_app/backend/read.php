<?php
    // hacer conexion
    include_once __DIR__.'/database.php';

    // SE CREA EL ARREGLO QUE SE VA A DEVOLVER EN FORMA DE JSON
    $data = array();
    // SE VERIFICA HABER RECIBIDO LA CADENA
    if( isset($_POST['search_cad']) ) {
        $cadena_search = $_POST['search_cad'];
        // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
        if ( $result = $conexion->query("SELECT * FROM productos WHERE nombre LIKE '%{$cadena_search}%' OR modelo LIKE '%{$cadena_search}%' OR detalles LIKE '%{$cadena_search}%'") ) {
            // SE OBTIENEN LOS RESULTADOS
			$row = $result->fetch_all(MYSQLI_ASSOC);

            if(!is_null($row)) {
                // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                foreach($row as $key => $value) {
                    foreach ($value as $key2 => $value2) {
                        $data[$key][$key2] = utf8_encode($value2);
                    }
                }
            }
			$result->free();
		} else {
            die('Query Error: '.mysqli_error($conexion));
        }
		$conexion->close();
    } 
    
    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($data, JSON_PRETTY_PRINT);
?>