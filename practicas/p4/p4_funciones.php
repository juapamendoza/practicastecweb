<?php
    //Declaracion de variables
    $numero = $_GET['num'];
    $numero2 = $_GET['num2'];
    
    ##########################
    # Funcion multiplo 5 y 7 #
    ##########################
    echo "<h3>FUNCION QUE DETERMINA SI UN NUMERO ES MULTIPLO DE 5 Y 7</h3>";
    echo "<p>Número dado: ".$numero."</p>";
    if ($numero % 5 == 0 && $numero % 7 == 0) {
        echo "<p><b>El número SÍ es múltiplo de 5 y 7</b></p>";
        echo "<br><br>";
    } else {
        echo "<p><b>El número NO es múltiplo de 5 y 7</b></p>";
        echo "<br><br>";
    }

    #############################
    # Funcion impar, par, impar #
    #############################
    echo "<h3>FUNCION QUE INGRESA DATOS A UNA MATRIZ HASTA QUE HAYA IMPAR, PAR, IMPAR</h3>";
    $arreglo = [[],[],[]];
    $n = 0; //aumenta con cada iteracion
    $totalnum = 0; //cuenta todos los numeros

    //ingresa numeros a la matriz
    do {
        $n1 = rand(1,50);
        $n2 = rand(1,50);
        $n3 = rand(1,50);
        $arreglo[0][] = $n1;
        $totalnum+=1;
        $arreglo[1][] = $n2;
        $totalnum+=1;
        $arreglo[2][] = $n3;
        $totalnum+=1;

        $n+=1;
    }while($n1 % 2 ==0 || $n2 % 2 !=0 || $n3 % 2 == 0);

    for($i=0; $i<$n; $i++){
        foreach($arreglo as $value )
        {
            echo "$value[$i],";
        }
        echo '<br>';
    }

    echo "<br><b>$totalnum numeros obtenidos de $n iteraciones</b>";

    ###########################################################
    # Funcion para encontrar el primer número entero obtenido #
    # aleatoriamente, y que además sea múltiplo de $num2      #
    ###########################################################
    echo "<h3>NUMERO ALEATORIO ENTERO Y MÚLTIPLO DE ".$numero2."</h3>";
    while (is_int($i=rand(1,50)) && $i % $numero2 != 0) {
        echo "<p>Numero aleatorio: ".$i."</p>";
    }
    echo "<p><b>Numero encontrado :) ".$i."</b></p><br>";

    //Variable del script anterior usando do-while
    echo "<h3>SCRIPT ANTERIOR REALIZADO CON DO-WHILE</h3>";
    do {
        $i=rand(1,50);
        echo "<p>Numero aleatorio: ".$i."</p>";
    } while (is_int($i) && $i % $numero2 != 0);
    echo "<p><b>Numero encontrado :) ".$i."</b></p><br>";

    #################################################
    # Funcion que crea un arreglo con el abecedario #
    #################################################
    for ($i=97; $i < 123; $i++) { 
        $arregloABC[$i] = chr($i);
    }
    echo "<h3>ARREGLO CON LAS LETRAS DEL ABECEDARIO</h3>";
    foreach ($arregloABC as $key => $value){
        echo '$arregloABC['.$key.'] -> '.$value."<br>";
    }

?>