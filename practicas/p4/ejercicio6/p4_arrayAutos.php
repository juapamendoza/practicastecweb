<?phP
    $arregloAutos = array(
        //Auto 1
        'ABC1234' => array(
            'Auto' => array(
                'marca' => 'HONDA',
                'modelo' => 2020,
                'tipo' => 'camioneta' ),
            'Propietario' => array(
                'nombre' => 'Juan P. Mendoza', 
                'ciudad' => 'Perote, Ver.',
                'direccion' => 'Agustin Melgar 78')),
        //Auto 2
        'UBN6339' => array(
            'Auto' => array(
                'marca' => 'MAZDA',
                'modelo' => 2019,
                'tipo' => 'sedan' ),
            'Propietario' => array(
                'nombre' => 'Ma. del Consuelo', 
                'ciudad' => 'Puebla, Pue.',
                'direccion' => '97 Oriente')),
        //Auto 3
        'LKU9856' => array(
            'Auto' => array(
                'marca' => 'TOYOTA',
                'modelo' => 2017,
                'tipo' => 'sedan' ),
            'Propietario' => array(
                'nombre' => 'Alma Mendoza', 
                'ciudad' => 'Xalapa, Ver.',
                'direccion' => 'Cerezas No. 68')),
        //Auto 4
        'REP1989' => array(
            'Auto' => array(
                'marca' => 'NISSAN',
                'modelo' => 2021,
                'tipo' => 'pickup' ),
            'Propietario' => array(
                'nombre' => 'Steven Dobermann', 
                'ciudad' => 'New York, NY.',
                'direccion' => '16 Street North')),
        //Auto 5
        'EBR0232' => array(
            'Auto' => array(
                'marca' => 'FORD',
                'modelo' => 2000,
                'tipo' => 'rural' ),
            'Propietario' => array(
                'nombre' => 'Gloria Armas', 
                'ciudad' => 'CDMX, MX.',
                'direccion' => 'Benito Juárez Norte 104')),
        //Auto 6
        'VXC1639' => array(
            'Auto' => array(
                'marca' => 'VOLKSWAGEN',
                'modelo' => 2022,
                'tipo' => 'camioneta' ),
            'Propietario' => array(
                'nombre' => 'Angel Ramírez', 
                'ciudad' => 'Los Angeles, CA.',
                'direccion' => 'Poplar Street 89')),
        //Auto 7
        'EOS8521' => array(
            'Auto' => array(
                'marca' => 'AUDI',
                'modelo' => 2023,
                'tipo' => 'sedan' ),
            'Propietario' => array(
                'nombre' => 'Juan C. Conde', 
                'ciudad' => 'Puebla, Pue.',
                'direccion' => '14 Oriente')),
        //Auto 8
        'MML1524' => array(
            'Auto' => array(
                'marca' => 'VOLVO',
                'modelo' => 2002,
                'tipo' => 'sedan' ),
            'Propietario' => array(
                'nombre' => 'Yésica Cruz', 
                'ciudad' => 'Tijuana, BC',
                'direccion' => 'Calle Castillo 45')),
        //Auto 9
        'FVN1022' => array(
            'Auto' => array(
                'marca' => 'FERRARI',
                'modelo' => 2015,
                'tipo' => 'deportivo' ),
            'Propietario' => array(
                'nombre' => 'José Gunson', 
                'ciudad' => 'Tenextepec, Ver.',
                'direccion' => 'Calle Arcoiris 51')),
        //Auto 10
        'SCP4826' => array(
            'Auto' => array(
                'marca' => 'VOLKSWAGEN',
                'modelo' => 1999,
                'tipo' => 'rural' ),
            'Propietario' => array(
                'nombre' => 'Silvestre Sondón', 
                'ciudad' => 'CDMX, MX.',
                'direccion' => 'Ponciano Arriaga 12')),
        //Auto 11
        'KUE5842' => array(
            'Auto' => array(
                'marca' => 'NISSAN',
                'modelo' => 2020,
                'tipo' => 'camioneta' ),
            'Propietario' => array(
                'nombre' => 'Eric Alemán', 
                'ciudad' => 'Los Cabos, BC',
                'direccion' => 'Roman Holiday 63')),
        //Auto 12
        'EGN6472' => array(
            'Auto' => array(
                'marca' => 'CADILLAC',
                'modelo' => 2021,
                'tipo' => 'sedan' ),
            'Propietario' => array(
                'nombre' => 'Taylor Swift', 
                'ciudad' => 'New York, NY.',
                'direccion' => 'Cornelia Street 34')),
        //Auto 13
        'YLF5684' => array(
            'Auto' => array(
                'marca' => 'FORD',
                'modelo' => 2007,
                'tipo' => 'rural' ),
            'Propietario' => array(
                'nombre' => 'Guillermo Ochoa', 
                'ciudad' => 'Guadalajara, Jal.',
                'direccion' => 'Circuito Presidentes 38')),
        //Auto 14
        'JGU9641' => array(
            'Auto' => array(
                'marca' => 'TESLA',
                'modelo' => 2023,
                'tipo' => 'sedan' ),
            'Propietario' => array(
                'nombre' => 'Elon Musk', 
                'ciudad' => 'Rindge, NH',
                'direccion' => 'Toothland Street 45')),
        //Auto 15
        'ONFQ5203' => array(
            'Auto' => array(
                'marca' => 'PORCHE',
                'modelo' => 2019,
                'tipo' => 'camioneta' ),
            'Propietario' => array(
                'nombre' => 'RuPaul', 
                'ciudad' => 'Orlando, FL.',
                'direccion' => 'Rainbow Road 67'))
    );

    $opcion = $_POST["opc"];
    $matricula = $_POST["mat"];

    if ($opcion == "matricula") {
        echo '<h3>Información del auto '.$matricula.'</h3>';
        foreach ($arregloAutos[$matricula] as $key => $value) {
            echo "<h4>";
            print_r($key);
            echo "</h4>";
            foreach($value as $key2 => $value2){
                echo "<b>";
                print_r($key2);
                echo ":</b>   ";
                print_r($value2);
                echo "<br>";
            }
        }
    } 
    elseif ($opcion == "todos"){
        echo '<h3>Todos los autos del arreglo</h3>';
        $i=0;
        foreach ($arregloAutos as $key => $value) {
            echo "<h4>".++$i.". ";
            print_r($key);
            echo "</h4>";
            foreach($value as $key2 => $value2){
                echo "<b>";
                print_r($key2);
                echo ":</b>";
                echo "<br>";
                foreach ($value2 as $key3 => $value3) {
                    echo "<b>";
                    print_r($key3);
                    echo ":</b>   ";
                    print_r($value3);
                    echo "<br>";
                }
            }
        }
    }
?>