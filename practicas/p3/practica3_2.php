<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <?php
            echo "<title>Práctica 3: Juan P. Mendoza</title>";
        ?>
    </head>
    <body>
        <?php
            echo "<p>------------------------</p>";
            $a = "0";
            $b = "true";
            $c = false;
            $d = ($a OR $b);
            $e = ($a AND $c);
            $f = ($a XOR $b);
            var_dump($a, $b, $c);
            var_dump($d, $e, $f);
            echo "<p>Uso de la funcion var_export para mostrar las variables c y e: </p>";
            echo "<p>Variable c: </p>";
            echo var_export($c, true);
            echo "<p>Variable e: </p>";
            echo var_export($e, true);
        ?>
    </body>
</html>