function verificar_nombre(nombre) {
    if (nombre.value == '') {
        let aviso = '<p style="color:red">El nombre no puede estar vacío<p>';
        $('#name_container').html(aviso);
        $('#name_container').show();
    }
    else{
        //$('#name_container').hide();
    }
}

function verificar_marca(marca) {
    if (marca.value == '') {
        let aviso = '<p style="color:red">Debes especificar un estilo musical<p>';
        $('#marca_container').html(aviso);
        $('#marca_container').show();
    }
    else{
        $('#marca_container').hide();
    }
}

function verificar_modelo(modelo) {
    if (modelo.value == '') {
        let aviso = '<p style="color:red">Debes especificar el artista<p>';
        $('#modelo_container').html(aviso);
        $('#modelo_container').show();
    }
    else{
        $('#modelo_container').hide();
    }
}

function verificar_precio(precio) {
    if (precio.value < 99.99) {
        let aviso = '<p style="color:red">El precio debe ser mayor a $99.99<p>';
        $('#precio_container').html(aviso);
        $('#precio_container').show();
    }
    else{
        $('#precio_container').hide();
    }
}

function verificar_detalles(detalles) {
    if (detalles.value == '') {
        detalles.value = 'N/A';
    }
}

function verificar_unidades(unidades) {
    if (unidades.value == '') {
        let aviso = '<p style="color:red">Falta especificar unidades<p>';
        $('#unidades_container').html(aviso);
        $('#unidades_container').show();
    }
    else if(unidades.value != ''){
        parseInt(unidades);
        if (unidades.value < 0) {
            let aviso = '<p style="color:red">Debes ingresar 0 o más unidades<p>';
            $('#unidades_container').html(aviso);
            $('#unidades_container').show();
        }
        else{
            $('#unidades_container').hide();
        }
    }
}

function verificar_img(img) {
    if (img.value == '') {
        img.value = 'img/imagen.png';
    }
}

$(document).ready(function() {
    let edit = false;
    console.log('jQuery is working');
    $('#product-result').hide();
    $('.verfi_alert').hide();
    fetchProducts();

    /*$('#name').keyup(function (e) {
        let search = $('#name').val();

            $.ajax({
                url: 'backend/product-search-name.php',
                type: 'GET',
                data: { search },
                success: function(response){
                    console.log(response);
                    if (response.length > 0) {
                        let aviso = '<p style="color:red">Ya existe un producto con ese nombre<p>';
                        $('#name_container').html(aviso);
                        $('#name_container').show();
                    }
                    else{
                        let aviso = '<p style="color:lime">Aun no hay un producto con ese nombre<p>';
                        $('#name_container').html(aviso);
                        $('#name_container').show();
                    }
                }
            })
        })*/

    $('#search').keyup(function (e) {
        if ($('#search').val()) {
            let search = $('#search').val();

            $.ajax({
                url: 'backend/product-search.php',
                type: 'GET',
                data: { search },
                success: function(response){
                    //console.log(response);
                    let productos = JSON.parse(response);
                    let template = '';
                    let template_bus = '';

                    productos.forEach(producto => {
                        template += `<li>
                            ${producto.nombre}
                        </li>`
                    });

                    $('#container').html(template);
                    $('#product-result').show();

                    productos.forEach(producto => {
                        template_bus += `
                            <tr productId="${producto.id}">
                                <td>${producto.id}</td>
                                <td>${producto.nombre}</td>
                                <td>${producto.marca}</td>
                                <td>${producto.modelo}</td>
                                <td>${producto.precio}</td>
                                <td>${producto.detalles}</td>
                                <td>${producto.unidades}</td>
                                <td>${producto.imagen}</td>
                                <td>
                                    <button class="product-delete btn btn-danger">
                                        Eliminar
                                    </button>    
                                </td>
                            </tr>
                        ` 
                    });
                    $('#products').html(template_bus);
                }
            });
        }
    });

    $('#product-form').submit(function (e) {
        //let datos = JSON.parse($('#description').val());
        const postData = {
            nombre: $('#name').val(),
            precio: $('#precioform').val(),
            unidades: $('#unidadesform').val(),
            modelo: $('#modeloform').val(),
            marca: $('#marcaform').val(),
            detalles: $('#detallesform').val(),
            imagen: $('#imagenform').val(),
            id: $('#product_id').val()
        };

        let url = edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';
        console.log(postData, url);

        productoJsonString = JSON.stringify(postData,null,2);
        console.log(productoJsonString);
        
        $.post(url, productoJsonString, function(response) {
            console.log(response);
            let respuesta = JSON.parse(response);
            fetchProducts();
            $('#product-form').trigger('reset'); 
            let msj = respuesta.message;
            alert(msj);
        });
        e.preventDefault;
    });

    function fetchProducts() {
        $.ajax({
            url: 'backend/product-list.php',
            type: 'GET',
            success: function (response) {
                let productos = JSON.parse(response);

                // verificar que el json tenga productos
                if(Object.keys(productos).length > 0){
                    let template = '';
    
                    productos.forEach(producto => {
                        template += `
                            <tr productId="${producto.id}">
                                <td>${producto.id}</td>
                                <td>
                                    <a href="#" class="product-item">${producto.nombre}</a>
                                </td>
                                <td>${producto.marca}</td>
                                <td>${producto.modelo}</td>
                                <td>${producto.precio}</td>
                                <td>${producto.detalles}</td>
                                <td>${producto.unidades}</td>
                                <td>${producto.imagen}</td>
                                <td>
                                    <button class="product-delete btn btn-danger">
                                        Eliminar
                                    </button>    
                                </td>
                            </tr>
                        `; 
                    });
                    $('#products').html(template);
                }
            }
        });
    }

    $(document).on('click', '.product-delete', function(){
        if (confirm('¿Quieres eliminar el producto?')) {
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');
            $.post('backend/product-delete.php', {id}, function(response){
                fetchProducts();
                console.log(response);
            });
        }
        let respuesta = JSON.parse(response);
        let msj = respuesta.message;
        alert(msj);
    });

    $(document).on('click', '.product-item', function(){
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');
        $.post('backend/product-single.php', {id}, function(response){
            const producto = JSON.parse(response);
            $('#name').val(producto.nombre);
            $('#marcaform').val(producto.marca);
            $('#modeloform').val(producto.modelo);
            $('#precioform').val(producto.precio);
            $('#detallesform').val(producto.detalles);
            $('#unidadesform').val(producto.unidades);
            $('#imagenform').val(producto.imagen);
            $('#product_id').val(producto.id);

            edit = true;
        })
    });
}); 