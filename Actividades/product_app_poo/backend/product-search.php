<?php
    use API\Productos as Productos;
    require_once __DIR__ . '/API/Productos.php';

    // SE VERIFICA HABER RECIBIDO LA BUSQUEDA
    if( isset($_GET['search']) ) {
        $search = $_GET['search'];
        
        $producto = new Productos('marketzone');
        $producto->search($search);
    } 
    
    $producto->get_response();
?>