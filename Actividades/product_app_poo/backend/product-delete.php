<?php
    use API\Productos as Productos;
    require_once __DIR__ . '/API/Productos.php';

    // SE VERIFICA HABER RECIBIDO EL ID
    if( isset($_POST['id']) ) {
        $id_obj = $_POST['id'];
        $producto = new Productos('marketzone');

        $producto->delete($id_obj);
    } 
    
    $producto->get_response();
?>