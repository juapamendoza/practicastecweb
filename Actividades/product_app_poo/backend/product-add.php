<?php
    use API\Productos as Productos;
    require_once __DIR__ . '/API/Productos.php';

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $info_producto = file_get_contents('php://input');
    
    $producto = new Productos('marketzone');
    $producto->add($info_producto);

    $producto->get_response();
?>