<?php
namespace API;
use mysqli;

    abstract class DataBase{
        protected $conexion;

        public function __construct($nombre_bd){
            $this->conexion = new mysqli("localhost", "root", "p0p0c1t4", "{$nombre_bd}");
            //echo 'Conexion establecida :)';
            if ($this->conexion->connect_errno) {
                echo 'No se puede conectar a la BD :( ' . $this->conexion->connect_error;
                exit;
            }
        }
    }

    //probar la conexion
    //$conex = new DataBase;
    //$conex->DataBase('marketzone');
?>