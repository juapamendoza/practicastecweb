<?php
    namespace API;
    use API\DataBase as DataBase;
    require_once __DIR__ . '/DataBase.php';

    class Productos extends DataBase{
        private $response;

        public function Productos($nombre_bd='marketzone'){
            $this->response = array();
            parent::__construct($nombre_bd);
        } //fin constructor

        public function add($objeto){
            //$producto = file_get_contents('php://input');
            $this->response = array(
                'status'  => 'error',
                'message' => 'Ya existe un producto con ese nombre'
            );
            if(!empty($objeto)) {
                // SE TRANSFORMA EL STRING DEL JASON A OBJETO
                $jsonOBJ = json_decode($objeto);
                // SE ASUME QUE LOS DATOS YA FUERON VALIDADOS ANTES DE ENVIARSE
                $sql = "SELECT * FROM productos WHERE nombre = '{$jsonOBJ->nombre}' AND eliminado = 0";
                $result = $this->conexion->query($sql);
                
                if ($result->num_rows == 0) {
                    $this->conexion->set_charset("utf8");
                    $sql = "INSERT INTO productos VALUES (null, '{$jsonOBJ->nombre}', '{$jsonOBJ->marca}', '{$jsonOBJ->modelo}', {$jsonOBJ->precio}, '{$jsonOBJ->detalles}', {$jsonOBJ->unidades}, '{$jsonOBJ->imagen}', 0)";
                    if($this->conexion->query($sql)){
                        $this->response['status'] =  "success";
                        $this->response['message'] =  "Producto agregado";
                    } else {
                        $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                    }
                }

                $result->free();
                $this->conexion->close();
            }
        } //fin funcion add

        public function delete($id){
            $this->response = array(
                'status'  => 'error',
                'message' => 'La consulta falló'
            );
            // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
            $sql = "UPDATE productos SET eliminado=1 WHERE id = {$id}";
            if ( $this->conexion->query($sql) ) {
                $this->response['status'] =  "success";
                $this->response['message'] =  "Producto eliminado";
            } else {
                $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
            }
            $this->conexion->close();
        } //fin funcion delete

        public function edit($objeto){
            //$producto = file_get_contents('php://input');
            $this->response = array(
                'status'  => 'error',
                'message' => 'La consulta falló'
            );
            if(!empty($objeto)) {
                // SE TRANSFORMA EL STRING DEL JASON A OBJETO
                $jsonOBJ = json_decode($objeto);

                $idobj = $jsonOBJ->id;
                $nombreobj = $jsonOBJ->nombre;
                $precioobj = $jsonOBJ->precio;
                $unidadesobj = $jsonOBJ->unidades;
                $modeloobj = $jsonOBJ->modelo;
                $marcaobj = $jsonOBJ->marca;
                $detallesobj = $jsonOBJ->detalles;
                $imagenobj = $jsonOBJ->imagen;

                // SE ASUME QUE LOS DATOS YA FUERON VALIDADOS ANTES DE ENVIARSE
                $sql = "UPDATE productos SET nombre='{$nombreobj}', precio='{$precioobj}', unidades='{$unidadesobj}', modelo='{$modeloobj}', marca='{$marcaobj}', detalles='{$detallesobj}', imagen='{$imagenobj}' WHERE id = '{$idobj}'";
                //$result = $this->conexion->query($sql);
                
                if ( $this->conexion->query($sql) ) {
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto modificado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
                // Cierra la conexion
                $this->conexion->close();
            }
        } //fin duncion edit

        public function list(){
            if ( $result = $this->conexion->query("SELECT * FROM productos WHERE eliminado = 0") ) {
                // SE OBTIENEN LOS RESULTADOS
                $rows = $result->fetch_all(MYSQLI_ASSOC);
        
                if(!is_null($rows)) {
                    // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
            $this->conexion->close();
        } //fin funcion list

        public function search($search){
            $sql = "SELECT * FROM productos WHERE (id = '{$search}' OR nombre LIKE '%{$search}%' OR marca LIKE '%{$search}%' OR detalles LIKE '%{$search}%') AND eliminado = 0";
            if ( $result = $this->conexion->query($sql) ) {
                // SE OBTIENEN LOS RESULTADOS
                $rows = $result->fetch_all(MYSQLI_ASSOC);

                if(!is_null($rows)) {
                    // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
            $this->conexion->close();
        } //fin funcion search

        public function single($id){
            $sql = "SELECT * FROM productos WHERE id = {$id}";
            if ( $result = $this->conexion->query($sql) ) {
                // SE OBTIENEN LOS RESULTADOS
                $rows = $result->fetch_array(MYSQLI_ASSOC);

                if(!is_null($rows)) {
                    // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                    foreach($rows as $num => $value) {
                        $this->response[$num] = utf8_encode($value);
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
            $this->conexion->close();
        } //fin funcion single

        public function get_response(){
            $respuesta_json = json_encode($this->response, JSON_PRETTY_PRINT);
            echo $respuesta_json;
        } //fin funcion get_response
    }
?>