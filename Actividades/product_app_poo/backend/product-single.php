<?php
    use API\Productos as Productos;
    require_once __DIR__ . '/API/Productos.php';

    // SE VERIFICA HABER RECIBIDO EL ID
    if( isset($_POST['id']) ) {
        $id = $_POST['id'];
        
        $producto = new Productos('marketzone');
        $producto->single($id);
    } 
    
    $producto->get_response();
?>