<?php
    use API\Productos as Productos;
    require_once __DIR__ . '/API/Productos.php';
    
    $conexion = new Productos('marketzone');

    if(!$conexion) {
        die('¡Base de datos NO conectada!');
    }
?>